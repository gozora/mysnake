# MySnake
My own implementation of famous Nokia game in C and Ncurses.

* Installation: `make`


* Run
  * Small size (6x8): `./mysnake --small`
  * Medium size (14x18): `./mysnake --medium`
  * Large size (full terminal size): `./mysnake --large`
  * Custom size (e.g. 20x40): `./mysnake --y-size=20 --x-size=40`


* Controls
  * Arrow or `w` `a` `s` `d` keys for snake control
  * `p`, `ESC` or `space bar` for pause

![hello](./pic/mysnake.png)
