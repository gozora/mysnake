/*
 * Copyright 2018 Vladimir (sodoma) Gozora <c(at)gozora.sk>
 *
 * This file is part of mysnake.
 *
 * mysnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * mysnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mysnake.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SNAKE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <panel.h>    // This includes ncurses.h

enum yx_e { y, x };
enum direction_e { N, E, S, W, dir_cnt };
enum windows_e { frame, game, status, full, win_cnt };
enum msg_type_e { victory, defeat, pause_game, help_game };
enum scale_e {small_sz = 1, medium_sz, large_sz};
enum colors_e
{
  // MC = My Color ;-)
  MC_WHITE_BLACK = 1,
  MC_WHITE_BLUE,
  MC_BLACK_WHITE,
  MC_RED_RED,
  MC_BLACK_RED,
  MC_WHITE_RED,
  MC_BLUE_BLUE,
  MC_BLACK_BLUE,
  MC_GREEN_GERRN,
  MC_BLACK_GREEN,
  MC_YELLOW_YELLOW,
  MC_BLACK_YELLOW,
  MC_BLACK_CYAN,
  MC_CYAN_CYAN
};

typedef int yx[2];
typedef enum err_e
{
  MY_ERR_OK,      // 0
  MY_ERR_WON,     // 1
  MY_ERR_LOST,    // 2
  MY_ERR_RESTART, // 3
  MY_ERR_MALLOC,  // 4
  MY_ERR_NDX,     // 5
  MY_ERR_WIN_PAN, // 6
  MY_ERR_QUIT,    // 7
  MY_ERR_SYNTAX   // 8
} myerr_t;

typedef struct lookup_table_s
{
  int *addr;
  int memb;
  size_t max_memb;
} lookup_table_t;

typedef struct food_s
{
  yx pos;
  bool eaten;
} food_t;

typedef struct screen_s
{
  int max;
  yx max_sz;
  WINDOW *win[win_cnt];
  PANEL *pan[win_cnt];
  bool colors;
  bool full_screen;
} screen_t;

typedef struct snake_s
{
  yx *body;
  int len;
  short speed;
  enum direction_e heading;
  lookup_table_t lt;
  food_t food;
  screen_t screen;
  unsigned int steps;
} snake_t;

#define SNAKE_H
#define VERSION "1.0.0"
#define SLEEP_START 500000
#define SLEEP_DEC 50000
#define SLEEP_MIN 100000
#define INC_AFTER 10
#define CHECK_RV(rc) if (rc != MY_ERR_OK) { return rc; }
#define FOOD_CHAR '*'
#define ESC_KEY 27
#ifndef PROGNAME
#define PROGNAME "snake"
#endif
#define UPDATE \
update_panels (); \
doupdate ()

// Function prototypes
int main_game (yx, bool*);
myerr_t init_snake_screen (screen_t*);
void cleanup_snake (snake_t*);
myerr_t copy_pos (yx, yx);
int handle_key_press (snake_t*, yx);
myerr_t handle_snake (snake_t*, yx);
void update_status (snake_t);
myerr_t msg_pop (snake_t s, enum msg_type_e);
myerr_t handle_options (yx, int, char*[]);
void msg_instructions (snake_t, bool*);

#ifdef DEBUG
  void _nc_free_and_exit(int);
#endif

#endif
