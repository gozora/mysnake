/*
 * Copyright 2018 Vladimir (sodoma) Gozora <c(at)gozora.sk>
 *
 * This file is part of mysnake.
 *
 * mysnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * mysnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mysnake.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mysnake.h"

extern myerr_t rv;

static void add_snake_body (snake_t*, yx);
static void build_lookupt (snake_t*);
static void calculate_direction (enum direction_e, yx);
static myerr_t check_collision (snake_t);
static myerr_t draw_food (snake_t*);
static int get_rand_num (unsigned int max);
static bool food_eaten (snake_t*, yx);
static void init_lookupt (lookup_table_t*, screen_t);
static void lookupt_to_yx (yx, int, int);
static int mycompar (const void*, const void*);
static void rand_pos (yx, screen_t, lookup_table_t*);
static int yx_to_lookupt (yx, int);
