/*
 * Copyright 2018 Vladimir (sodoma) Gozora <c(at)gozora.sk>
 *
 * This file is part of mysnake.
 *
 * mysnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * mysnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mysnake.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "msg.h"

void update_status (snake_t s)
{
  WINDOW *status_win = s.screen.win[status];

  wmove (status_win, 0, 0);
  // wprintw (status_win, "Y: %d, X: %d ", s.body[0][y], s.body[0][x]);
  wprintw (status_win, "Speed: %d ", s.speed);
  wprintw (status_win, "Length:%4d ", s.len);
  wprintw (status_win, "Distance: %d ", s.steps);
}

myerr_t msg_pop (snake_t s, enum msg_type_e msg_id)
{
  int ch = 0;
  yx max_sz = {0, 0};
  yx center = {0, 0};
  getmaxyx (s.screen.win[full], max_sz[y], max_sz[x]);
  // Define window size for each message.
  const yx sz = {5, max_sz[x] - 4};
  // Define messages to display.
  char *messages[] = {"Victory!", "Defeat!", "Pause",
                      "Welcome to my snake game!"};
  char *instructions[] = { "Press 'r' to restart or 'q' to quit.",
    "Press 'r' to restart or 'q' to quit.",
    "Press 'r' to restart, 'q' to quit or 'p' to unpause.",
    "Control with arrow keys or 'wasd', pause 'p'. Press 's' to start." };

  // Calculate center of the screen, where pop-up will be displayed.
  center[y] = (max_sz[y] / 2) - (sz[y] / 2);
  center[x] = (max_sz[x] / 2) - (sz[x] / 2);

  WINDOW *msg_win = newwin (sz[y], sz[x], center[y], center[x]);
  PANEL *msg_pan = new_panel (msg_win);

 /*
  * Enable keypad, otherwise after message windows is displayed and closed,
  * buffer of game windows will receive 0x1b 0x5b 0x41 sequence
  * (vt100 arrow codes) (for whatever reason) and snake control will become
  * unresponsive for couple of cycles.
  */
  keypad (msg_win, TRUE);
  box (msg_win, 0, 0);

  // Change color of pop-up windows.
  if (s.screen.colors)
  {
    switch (msg_id)
    {
      case victory:
        wbkgd (msg_win, COLOR_PAIR (MC_BLACK_YELLOW));
      break;
      case defeat:
        wbkgd (msg_win, COLOR_PAIR (MC_WHITE_RED));
      break;
      case help_game:
      case pause_game:
        wbkgd (msg_win, COLOR_PAIR (MC_BLACK_BLUE));
      break;
    }
  }

  print_middle (msg_win, 1, messages[msg_id]);
  print_middle (msg_win, 3, instructions[msg_id]);

  top_panel (msg_pan);
  UPDATE;

  while ((ch = wgetch (msg_win)))
  {
    if (ch == 'r' || ch == 's')
    {
      rv = MY_ERR_RESTART;
      break;
    }
    else if (ch == 'q')
    {
      rv = MY_ERR_QUIT;
      break;
    }
    else if ((ch == 'p' || ch == ESC_KEY) &&
             (msg_id == pause_game || msg_id == help_game))
    {
      rv = MY_ERR_OK;
      break;
    }
  }

  bottom_panel (msg_pan);
  del_panel (msg_pan);
  delwin (msg_win);

  UPDATE;

  return rv;
}

void msg_instructions (snake_t s, bool *show_help)
{
  if (*show_help)
  {
    msg_pop (s, help_game);

    *show_help = FALSE;
  }
}

static void print_middle (WINDOW *win, int line, char *msg)
{
  yx sz = {0, 0};
  size_t len = strlen (msg);
  getmaxyx (win, sz[y], sz[x]);

  mvwprintw (win, line, (sz[x] / 2) - (len / 2), msg);
}
