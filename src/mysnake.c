/*
 * Copyright 2018 Vladimir (sodoma) Gozora <c(at)gozora.sk>
 *
 * This file is part of mysnake.
 *
 * mysnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * mysnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mysnake.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "mysnake.h"

// Global variables
myerr_t rv = MY_ERR_OK;

int main (int argc, char *argv[])
{
  yx req_sz = {-1, -1};
  bool show_help = TRUE;

  if ((rv = handle_options (req_sz, argc, argv)) != MY_ERR_OK)
    return rv;

  while (TRUE)
  {
    rv = main_game (req_sz, &show_help);

    if (rv != MY_ERR_RESTART)
      break;
  }

  #ifdef DEBUG
    _nc_free_and_exit (rv);
  #endif

  return rv;
}

int main_game (yx sz, bool *show_help)
{
  int ch = 0;
  yx pos_cur = {0, 0};
  useconds_t slp = 500000;
  putenv ("ESCDELAY=0");
  snake_t s;

  memset (&s, '\0', sizeof (snake_t));

  s.screen.max_sz[y] = sz[y];
  s.screen.max_sz[x] = sz[x];

  /*
   * Initialize ncurses and create basic game layout.
   * This function also sets maximum number of screen elements (screen.max)
   * in screen.win[game].
   */
  if ((rv = init_snake_screen (&s.screen)) != MY_ERR_OK)
    goto cleanup;

  /* Variable initialization. */
  srand (time (NULL));          // Initialize RNG.

  // Allocate maximum possible size for snake and set heading direction.
  s.body = calloc (s.screen.max, sizeof (yx));
  s.heading = dir_cnt;
  s.len = 0;
  s.food.eaten = TRUE;
  s.speed = 1;

  // Initialize lookup table
  s.lt.addr = calloc (s.screen.max, sizeof (int));
  s.lt.memb = 0;
  s.lt.max_memb = 0;

  if (s.body == NULL || s.lt.addr == NULL)
    rv = MY_ERR_MALLOC;

  // Set starting point
  pos_cur[y] = s.screen.max_sz[y] / 2;
  pos_cur[x] = s.screen.max_sz[x] / 2;
  copy_pos (s.body[s.len++], pos_cur);

  msg_instructions (s, show_help);

  if (rv != MY_ERR_OK)
    goto cleanup;

  while (TRUE)
  {
    ch = handle_key_press (&s, pos_cur);

    if (ch == 'p' || ch == ESC_KEY || ch == ' ')
    {
      rv = msg_pop (s, pause_game);

      if (rv == MY_ERR_RESTART || rv == MY_ERR_QUIT)
        break;
    }

    if ((rv = handle_snake (&s, pos_cur)) != MY_ERR_OK)
      break;

    update_status (s);

    UPDATE;

    // If 10 pieces of food is eaten, increment game speed.
    // s.food.eaten == TRUE -> speed gets decremented only once / 10 food.
    // slp > 100000         -> maximum available speed.
    if (s.len % INC_AFTER == 0 && s.food.eaten == TRUE && slp > SLEEP_MIN)
    {
      slp = SLEEP_START - (s.speed * SLEEP_DEC);
      s.speed++;
    }

    usleep (slp);
  }

  if (rv == MY_ERR_WON)
  {
    s.len++;
    update_status (s);
    UPDATE;

    rv = msg_pop (s, victory);
  }
  else if (rv == MY_ERR_LOST)
    rv = msg_pop (s, defeat);

  cleanup:
  cleanup_snake (&s);

  return rv;
}
