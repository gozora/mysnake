/*
 * Copyright 2018 Vladimir (sodoma) Gozora <c(at)gozora.sk>
 *
 * This file is part of mysnake.
 *
 * mysnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * mysnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mysnake.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "init_cleanup.h"

void cleanup_snake (snake_t *s)
{
  int i;

  if (s->body)
    free (s->body);

  if (s->lt.addr)
    free (s->lt.addr);

  for (i = win_cnt - 1; i >= 0; i--)
  {
    del_panel (s->screen.pan[i]);
    delwin (s->screen.win[i]);
  }

  endwin ();
}

myerr_t init_snake_screen (screen_t *screen)
{
  int i;

  initscr ();                 // Initialize ncurses.
  curs_set (0);               // Hide cursor.
  noecho ();                  // Do no output characters.
  raw ();                     // Do not interpret sequences like ^q, ^z, ^c ...

  // Enable colors if terminal has such capability, and initialize color pairs.
  screen->colors = initialize_colors ();

  // If snake play area was not explicitly defined, create maximum possible.
  if (screen->max_sz[y] == 0 || screen->max_sz[x] == 0)
  {
    getmaxyx (stdscr, screen->max_sz[y], screen->max_sz[x]);
    screen->max_sz[y]--;
    screen->full_screen = TRUE;
  }

  draw_snake_layout (screen); // Create basic game layout.

  for (i = 0; i < win_cnt; i++)
  {
    if (screen->win[i] == NULL || screen->pan[i] == NULL)
      return MY_ERR_WIN_PAN;
  }

  return MY_ERR_OK;
}

static void draw_snake_layout (screen_t *scr)
{
  // Just a shortcut.
  yx win_sz = { scr->max_sz[y], scr->max_sz[x] }; // Size of game window.
  yx real_win_sz = {0, 0}; // Full size of terminal window.
  yx scr_mid = {0, 0};     // Approximate middle of terminal window.

  // Create windows
  scr->win[full] = newwin (0, 0, 0, 0);
  getmaxyx (scr->win[full], real_win_sz[y], real_win_sz[x]);

  if (scr->full_screen)
  {
    scr_mid[y] = 0;
    scr_mid[x] = 0;
  }
  else
  {
    scr_mid[y] = (real_win_sz[y] / 2) - (win_sz[y] / 2);
    scr_mid[x] = (real_win_sz[x] / 2) - (win_sz[x] / 2);
  }

  scr->win[frame] = newwin (win_sz[y], win_sz[x], scr_mid[y], scr_mid[x]);
  scr->win[game] = derwin (scr->win[frame], win_sz[y] - 2, win_sz[x] - 2, 1, 1);
  scr->win[status] = newwin (1, real_win_sz[x], real_win_sz[y] - 1, 0);

  // Create panels
  scr->pan[frame] = new_panel (scr->win[frame]);
  scr->pan[game] = new_panel (scr->win[game]);
  scr->pan[status] = new_panel (scr->win[status]);
  scr->pan[full] = new_panel (scr->win[full]);

  // Set basic style if terminal supports colors
  if (scr->colors)
  {
    wbkgd (scr->win[frame], COLOR_PAIR (MC_WHITE_BLACK));
    wbkgd (scr->win[game], COLOR_PAIR (MC_BLACK_GREEN));
    wbkgd (scr->win[status], COLOR_PAIR (MC_WHITE_BLUE));
    wbkgd (scr->win[full], COLOR_PAIR (MC_CYAN_CYAN));
  }

  bottom_panel (scr->pan[full]);

  keypad (scr->win[game], TRUE);  // Enable special keys (arrows) interpretation.
  nodelay (scr->win[game], TRUE); // Disable getch () blocking.
  box (scr->win[frame], 0, 0);    // Draw frame

  getmaxyx (scr->win[game], scr->max_sz[y], scr->max_sz[x]);
  scr->max = scr->max_sz[y] * scr->max_sz[x];

  UPDATE;
}

static bool initialize_colors (void)
{
  bool have_colors = FALSE;

  if (has_colors ())
  {
    start_color ();

    init_pair (MC_WHITE_BLACK, COLOR_WHITE, COLOR_BLACK);
    init_pair (MC_WHITE_BLUE, COLOR_WHITE, COLOR_BLUE);
    init_pair (MC_BLACK_WHITE, COLOR_BLACK, COLOR_WHITE);
    init_pair (MC_RED_RED, COLOR_RED, COLOR_RED);
    init_pair (MC_BLACK_RED, COLOR_BLACK, COLOR_RED);
    init_pair (MC_WHITE_RED, COLOR_WHITE, COLOR_RED);
    init_pair (MC_BLUE_BLUE, COLOR_BLUE, COLOR_BLUE);
    init_pair (MC_BLACK_BLUE, COLOR_BLACK, COLOR_BLUE);
    init_pair (MC_GREEN_GERRN, COLOR_GREEN, COLOR_GREEN);
    init_pair (MC_BLACK_GREEN, COLOR_BLACK, COLOR_GREEN);
    init_pair (MC_YELLOW_YELLOW, COLOR_YELLOW, COLOR_YELLOW);
    init_pair (MC_BLACK_YELLOW, COLOR_BLACK, COLOR_YELLOW);
    init_pair (MC_BLACK_CYAN, COLOR_BLACK, COLOR_CYAN);
    init_pair (MC_CYAN_CYAN, COLOR_CYAN, COLOR_CYAN);

    have_colors = TRUE;
  }

  return have_colors;
}
