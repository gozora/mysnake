/*
 * Copyright 2018 Vladimir (sodoma) Gozora <c(at)gozora.sk>
 *
 * This file is part of mysnake.
 *
 * mysnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * mysnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mysnake.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "helper_functions.h"

myerr_t handle_options (yx req_sz, int argc, char *argv[])
{
  int c;
  static int predefined_sz;
  bool end = FALSE;
  static struct option long_opt[] =
  {
    { "small"   , no_argument      , &predefined_sz, small_sz },
    { "medium"  , no_argument      , &predefined_sz, medium_sz },
    { "large"   , no_argument      , &predefined_sz, large_sz },
    { "help"    , no_argument      , 0, 'h' },
    { "version" , no_argument      , 0, 'v' },
    { "x-size"  , required_argument, 0, 'x' },
    { "y-size"  , required_argument, 0, 'y' },
    { 0         , 0                , 0, 0   },
  };

  while (TRUE)
  {
    int option_ndx = 0;
    c = getopt_long (argc, argv, "smlfhvx:y:", long_opt, &option_ndx);

    if (argc == 1)
    {
      predefined_sz = small_sz;
      break;
    }

    if (c == -1 || c == 0)
      break;

    switch (c)
    {
      case 'h':
        return show_help ();
      break;
      case 'v':
        return show_version ();
      break;
      case 's':
        predefined_sz = small_sz;
        end = TRUE;
      break;
      case 'm':
        predefined_sz = medium_sz;
        end = TRUE;
      break;
      case 'l':
        predefined_sz = large_sz;
        end = TRUE;
      break;
      case 'x':
        req_sz[x] = atoi (optarg);
      break;
      case 'y':
        req_sz[y] = atoi (optarg);
      break;
      default:
        return bad_syntax (argv[0]);
      break;
    }

    if (end)
      break;
  }

  if (predefined_sz)
  {
    switch (predefined_sz)
    {
      case small_sz:
        req_sz[y] = 8;
        req_sz[x] = 10;
      break;
      case medium_sz:
        req_sz[y] = 16;
        req_sz[x] = 20;
      break;
      case large_sz:
        req_sz[y] = 0;
        req_sz[x] = 0;
      break;
    }
  }
  else
  {
    if (req_sz[y] <= 0 || req_sz[x] <= 0)
      return bad_syntax (argv[0]);
  }

  return MY_ERR_OK;
}

static myerr_t show_version ()
{
  printf ("%s %s\n", PROGNAME, VERSION);
  return MY_ERR_OK;
}

static myerr_t show_help ()
{
  printf("\
  Usage: \n\
    %s -s|-m|-l\n\
    %s -y NUM -x NUM\n\
    %s -h\n\
    %s -v\n\
  \n\
  Remake of Snake game in ncurses.\n\
  \n\
  Arguments:\n\
    -s, --small                     small game area (6x8)\n\
    -m, --medium                    medium game area (14x18)\n\
    -l, --large                     large game area (fullscreen)\n\
    -y, --y-size=NUM                custom sized area with NUM columns\n\
    -x, --x-size=NUM                custom sized area with NUM rows\n\
    -h, --help                      this screen\n\
    -v, --version                   display version\n\
  \n", PROGNAME, PROGNAME, PROGNAME, PROGNAME);

  return MY_ERR_OK;
}

static myerr_t bad_syntax (char *prog)
{
  printf("%s: Bad syntax.\n", prog);
  printf("Try '%s --help' for more information.\n", prog);

  return MY_ERR_SYNTAX;
}
