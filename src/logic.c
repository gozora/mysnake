/*
 * Copyright 2018 Vladimir (sodoma) Gozora <c(at)gozora.sk>
 *
 * This file is part of mysnake.
 *
 * mysnake is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * mysnake is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with mysnake.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "logic.h"

myerr_t copy_pos (yx pos_dst, yx pos_src)
{
  CHECK_RV (rv);

  pos_dst[y] = pos_src[y];
  pos_dst[x] = pos_src[x];

  return MY_ERR_OK;
}

myerr_t handle_snake (snake_t *s, yx pos_cur)
{
  int i;
  myerr_t rv = MY_ERR_OK;
  WINDOW *win = s->screen.win[game];

  // Add current position into snake body
  add_snake_body (s, pos_cur);

  // Draw snake body
  for (i = 0; i < s->len; i++)
    mvwaddch (win, s->body[i][y], s->body[i][x], ACS_DIAMOND);

  // Did we crashed ?
  if ((rv = check_collision (*s)) == MY_ERR_LOST)
    return rv;

  // Prepare map for drawing of food.
  build_lookupt (s);

  // Remove snake tail
  mvwaddch (win, s->body[s->len][y], s->body[s->len][x], ' ');
  s->steps++;

  // Draw food and check if we have won ...
  rv = draw_food (s);

  s->food.eaten = food_eaten (s, pos_cur);

  return rv;
}

int handle_key_press (snake_t *s, yx pos_cur)
{
  WINDOW *win = s->screen.win[game];
  int ch = wgetch (win);

  if (s->heading == dir_cnt)
    s->heading = get_rand_num (dir_cnt);

  switch (ch)
  {
    case 'w':
    case KEY_UP:
      if (s->len == 1 || s->heading != S)
        s->heading = N;
    break;
    case 's':
    case KEY_DOWN:
      if (s->len == 1 || s->heading != N)
        s->heading = S;
    break;
    case 'a':
    case KEY_LEFT:
      if (s->len == 1 || s->heading != E)
        s->heading = W;
    break;
    case 'd':
    case KEY_RIGHT:
      if (s->len == 1 || s->heading != W)
        s->heading = E;
    break;
  }

  calculate_direction (s->heading, pos_cur);

  return ch;
}

static myerr_t draw_food (snake_t *s)
{
  CHECK_RV (rv);
  WINDOW *win = s->screen.win[game];

  if (s->screen.max - 1 == s->len)
    return MY_ERR_WON;

  if (s->food.eaten == TRUE)
  {
    rand_pos (s->food.pos, s->screen, &s->lt);
    mvwaddch (win, s->food.pos[y], s->food.pos[x], FOOD_CHAR);
  }

  return MY_ERR_OK;
}

static bool food_eaten (snake_t *s, yx pos_cur)
{
  if (s->food.pos[y] == pos_cur[y] && s->food.pos[x] == pos_cur[x])
  {
    s->len++;
    return TRUE;
  }
  else
  {
    return FALSE;
  }
}

static void init_lookupt (lookup_table_t *lt, screen_t screen)
{
  int i;
  lt->memb = screen.max;

  for (i = 0; i < lt->memb; i++)
    lt->addr[i] = i;

  lt->memb--;
  lt->max_memb = lt->memb;
}

static void calculate_direction (enum direction_e heading, yx pos_cur)
{
  switch (heading)
  {
    case N:
      pos_cur[y]--;
    break;
    case E:
      pos_cur[x]++;
    break;
    case S:
      pos_cur[y]++;
    break;
    case W:
      pos_cur[x]--;
    break;
    default:
      // dir_cnt is not a real direction
    break;
  }
}

static int get_rand_num (unsigned int max)
{
  int rand_num;

  rand_num = rand ();

  if (max > 0)
    rand_num %= max;

  return rand_num;
}

static int yx_to_lookupt (yx pos, int max_x)
{
  return (pos[y] * max_x) + pos[x];
}

static void lookupt_to_yx (yx pos, int lookupt_num, int max_x)
{
  pos[y] = lookupt_num / max_x;
  pos[x] = lookupt_num % max_x;
}

static int mycompar (const void *p1, const void *p2)
{
  return (*(int*) p2 - *(int*) p1);
}

static void build_lookupt (snake_t *snake)
{
  init_lookupt (&snake->lt, snake->screen);

  int ndx[snake->len], i;

  for (i = 0; i < snake->len; i++)
    ndx[i] = yx_to_lookupt (snake->body[i], snake->screen.max_sz[x]);

  qsort (ndx, i, sizeof (int), mycompar);

  for (i = 0; i < snake->len; i++)
  {
    if (ndx[i] == snake->lt.max_memb)
      continue;

    memmove (snake->lt.addr + ndx[i], snake->lt.addr + ndx[i] + 1,
             (snake->lt.memb - ndx[i]) * sizeof (int));
  }

  snake->lt.memb -= snake->len;
}

static void rand_pos (yx pos, screen_t screen, lookup_table_t *lt)
{
  int rnum;

  if (lt)
  {
    rnum = get_rand_num (lt->memb);

    lookupt_to_yx (pos, lt->addr[rnum], screen.max_sz[x]);
  }
  else
  {
    rnum = get_rand_num (0);

    if (pos)
    {
      pos[y] = rnum % screen.max_sz[y];
      pos[x] = rnum % screen.max_sz[x];
    }
  }
}

static void add_snake_body (snake_t *s, yx pos_cur)
{
  int h_ndx = s->len - 1;

  while (h_ndx >= 0)
  {
    copy_pos (s->body[h_ndx + 1], s->body[h_ndx]);
    h_ndx--;
  }

  copy_pos (s->body[0], pos_cur);
}

static myerr_t check_collision (snake_t s)
{
  myerr_t rv = MY_ERR_OK;
  int head_pos = yx_to_lookupt (s.body[0], s.screen.max_sz[x]);
  int i;

  if (s.body[0][y] > s.screen.max_sz[y] - 1 || \
      s.body[0][x] > s.screen.max_sz[x] - 1 || \
      s.body[0][y] < 0 || s.body[0][x] < 0)
  {
    return MY_ERR_LOST;
  }

  for (i = 1; i < s.len; i++)
  {
    if (head_pos == yx_to_lookupt (s.body[i], s.screen.max_sz[x]))
    {
      rv = MY_ERR_LOST;
      break;
    }
  }

  return rv;
}
