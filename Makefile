NAME = mysnake
CC = gcc
CFLAGS += -no-pie -O0 -g3 -Wall -Iinclude -DPROGNAME=\"$(NAME)\"
ifeq ($(DEBUG),1)
	CFLAGS += -I/home/sodoma/devel/projects/ncurses-6.0/include \
-L/home/sodoma/devel/projects/ncurses-6.0/lib -DDEBUG
endif

LDFLAGS += -lpanel -lncurses
SRC = $(wildcard src/*.c)
OBJ = $(SRC:.c=.o)
DEP = $(OBJ:.o=.d)


.PHONY: clean info

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) $(LDFLAGS)

-include $(DEP)
%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@
	$(CC) $(CFLAGS) -MM -MT $@ -MF $*.d $<

info:
	$(info $(SRC))
	$(info $(OBJ))
	$(info $(DEP))
	$(info $(COMPILE.c))
	$(info $(DEBUG))

clean:
	rm -f $(OBJ)
	rm -f $(DEP)
	rm -f $(NAME)
